Install:

- With Drush: drush en gdpr_backup_cleaner -y

Requirements:

- Backup and Migrate (7.x-3.6)
- General Data Protection Regulation

**Important!** This module is integrated with "backup and migrate" project, that's why we have some restrictions regarding making and
restoring backups. This module only works with backups, created via "backup and migrate" project.

After installation:

 1. Create new database and relevant database source here 'admin/config/system/backup_migrate/settings/source'

 2. go to the module configuration page  'admin/config/gdpr/backup-cleaner-settings' and set relevant database key.

Now your GDPR task will be performed for existing database backups.
